const rp = require('request-promise').defaults({json: true});
const qs = require('qs');

class EasyOnlineSim {
    /**
     * Создает объект взаимодействия с onlinesim
     * @param {Object} params объект с настройками
     * @param {string} params.token токен для апи
     * @param {string} params.requestInterval пауза в милисекундах между опросами апи для получения смc
     * @example

      const EasyOnlineSim = require('easy-online-sim');
      const eOS = new EasyOnlineSim({token: "****" , requestInterval: 5000 });
      eOS.getNumInstagram()
           .then(tzIdObj => {
               // операции с номером tzIdOb.number
               return eOS.getState(tzIdObj.id);
           })
           .then(tzIdObjData => {
               console.log(tzIdObjData.msg) // <-- код здесь
               return eOS.done(tzIdObjData.id);
           })
           .then(responseDone => console.log(responseDone)
           .catch(console.error);
      * @return <Object> экземпляр объекта EasyOnlineSim
     */
    constructor(params) {
        this.uri = `http://onlinesim.ru/api`;
        this.config = {
            getNum: {
                form: 1
            },
            getState: {
                message_to_code: 1
            }
        };
        const {token, login, password, requestInterval} = params;

        this.requestInterval = requestInterval | 1500;
        if (token) this.token = token;
        else {
            // операции по получению токена, будут в след версиях, лень
        }
    }

    /**
     * Приватный метод для создания интервалов между запросами, делает вызов колбека после истечения заданного времени
     * @param {number} requestInterval интервал времени в милисекундах, после которого следует вызвать колбек
     * @param {function} callback - функция, которую следует вызвать после прохождения интервала
     * @param {Object|string|number} params - параметры вызываемого колбека
     * @returns {Promise.<TResult>} возвращает промис
     * @private
     */
    _pause(requestInterval, callback, params = {}) {
        let self = this;
        return new Promise((resolve) => {
            setTimeout(resolve, requestInterval, true);
        }).then(response => callback.call(self, params));
    }

    /**
     * Приватный метод для формирования url строки, миксует в параметры данные заданные пользователем, встроенные параметры класса и токен
     * @param {string} methodName - имя метода онлайнсим (без .php)
     * @param {Object} params - объект параметров для урл запроса
     * @returns {string} сформированная строка запроса
     * @private
     */
    _genUrl(methodName, params) {
        if (this.config[methodName] instanceof Object) {
            params = Object.assign({}, this.config[methodName], params);
        }
        let mixedParams = Object.assign({}, {apikey: this.token}, params);
        let paramsStringify = qs.stringify(mixedParams);
        return `${this.uri}/${methodName}.php?${paramsStringify}`
    }

    /**
     * Приватный метод обертка для реквеста, выполняет ET запрос по сформированной урл строке, метод не предусматривает иных настроек, ответ приходит в формате Js object
     * @param {string} url - сформированная строка запроса
     * @returns {Promise.<object>} - объект с данными ответа
     * @private
     */
    _request(url) {
        return rp(url)
            .then(response => response);
    }

    /**
     * Метод запроса номера для определенного сервиса у онлайнсим, для получения номера телефона необходимо вызвать метод getState(tzId, false) false передает ответ о номере , но не повторяет запросы для прочтения пришедшего на него смс
     * @method getNum
     * @param {string} service название сервиса
     * @link http://onlinesim.ru/docs/api/ru#getnum8
     * @returns {Promise.<0bject>} объект с tzid, для получения по tzid
     */
    getNum(service) {
        return this._request(this._genUrl('getNum', {service: service}))
            .then(response => {
                console.log('getNum', response, typeof response, response.response);
                if (response.response == 1) {
                    return response.tzid;
                }
                else throw new Error(JSON.stringify(response));

            })
    }

    /**
     * Запрос номера в onlinesim
     * @method getNumInstagram
     * @returns {Promise.<object.<string, number, string>>} { type: 'number', id: '3686165', number: '+79681956382' }
     */
    getNumInstagram() {
        let self = this;
        return this.getNum('Instagram')
            .then(id => self.getState(id, false))

    }

    /**
     * Метод, позволяющий получить состояние по запршенной операции, повторяется циклично пока не будет прочитано смс, таймут берется из конструктора класса
     *@method getState
     * @param {number} id tzid выданный на сервис+номер для работы с сервисом
     * @param {bool} repeat флаг необходимости повтора запросов
     * @returns {Promise.<TResult>}
     */
    getState(id, repeat = true) {
        let self = this;
        return this._request(this._genUrl('getState', {tzid: id}))
            .then(response => {
                response = response[0];
                console.log('getState response:', response);
                if (response.response == 1) {

                    return {type: 'code', id: response.tzid, msg: response.msg};

                } else if (response.response == 'TZ_NUM_WAIT' && repeat) {

                    console.log('pause and repeat');
                    return this._pause(self.requestInterval, self.getState, id);

                } else if (response.response == 'TZ_NUM_WAIT') {

                    return {type: 'number', id: response.tzid, number: response.number};

                }
                else {
                    console.log('pause and repeat');
                    return this._pause(self.requestInterval, self.getState, id);
                    //throw new Error(JSON.stringify(response));
                }
            })
    }

    /**
     * Метод выполняет закрытие операции с положительным результатом, под капотом setOperationOk
     * @method done
     * @param {number} id tzid выданный на сервис+номер для работы с сервисом
     * @returns {Promise.<TResult>}
     */
    done(id) {
        return this._request(this._genUrl('setOperationOk', {tzid: id}))
            .then(response => {
                if (response.response == 1) {
                    return response.tzid;
                } else {
                    throw new Error(JSON.stringify(response));
                }
            })
    }

    /**
     * Метод выполняет закрытие операции с тикетом о неверных данных, под капотом setOperationRevise
     * @method abort
     * @param {number} id tzid выданный на сервис+номер для работы с сервисом
     * @returns {Promise.<TResult>}
     */
    abort(id) {
        return this._request(this._genUrl('setOperationRevise', {tzid: id}))
            .then(response => {
                if (response.response == 1) {
                    return response.tzid;
                } else {
                    throw new Error(JSON.stringify(response));
                }
            })
    }

    /**
     * Метод запрашивает баланс по кабинету, при успешном выполнении запроса response = 1
     * @method balance
     * @returns {Promise.<Object>} объект с данными по балансу {response:RESPONSE, "balance":"BALANCE", "zbalance":"ZBALANCE"}
     */
    balance() {
        return this._request(this._genUrl('getBalance'))
    }
}

module.exports = EasyOnlineSim;







