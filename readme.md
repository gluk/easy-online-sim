## Classes

<dl>
<dt><a href="#EasyOnlineSim">EasyOnlineSim</a></dt>
<dd></dd>
</dl>

## Functions

<dl>
<dt><a href="#getNum">getNum(service)</a> ⇒ <code>Promise.&lt;0bject&gt;</code></dt>
<dd><p>Метод запроса номера для определенного сервиса у онлайнсим, для получения номера телефона необходимо вызвать метод getState(tzId, false) false передает ответ о номере , но не повторяет запросы для прочтения пришедшего на него смс</p>
</dd>
<dt><a href="#getNumInstagram">getNumInstagram()</a> ⇒ <code>Promise.&lt;object.&lt;string, number, string&gt;&gt;</code></dt>
<dd><p>Запрос номера в onlinesim</p>
</dd>
<dt><a href="#getState">getState(id, repeat)</a> ⇒ <code>Promise.&lt;TResult&gt;</code></dt>
<dd><p>Метод, позволяющий получить состояние по запршенной операции, повторяется циклично пока не будет прочитано смс, таймут берется из конструктора класса</p>
</dd>
<dt><a href="#done">done(id)</a> ⇒ <code>Promise.&lt;TResult&gt;</code></dt>
<dd><p>Метод выполняет закрытие операции с положительным результатом, под капотом setOperationOk</p>
</dd>
<dt><a href="#abort">abort(id)</a> ⇒ <code>Promise.&lt;TResult&gt;</code></dt>
<dd><p>Метод выполняет закрытие операции с тикетом о неверных данных, под капотом setOperationRevise</p>
</dd>
<dt><a href="#balance">balance()</a> ⇒ <code>Promise.&lt;Object&gt;</code></dt>
<dd><p>Метод запрашивает баланс по кабинету, при успешном выполнении запроса response = 1</p>
</dd>
</dl>

<a name="EasyOnlineSim"></a>

## EasyOnlineSim
**Kind**: global class  
<a name="new_EasyOnlineSim_new"></a>

### new EasyOnlineSim(params)
Создает объект взаимодействия с onlinesim

**Returns**: <Object> экземпляр объекта EasyOnlineSim  

| Param | Type | Description |
| --- | --- | --- |
| params | <code>Object</code> | объект с настройками |
| params.token | <code>string</code> | токен для апи |
| params.requestInterval | <code>string</code> | пауза в милисекундах между опросами апи для получения смc |

**Example**  
```js
const EasyOnlineSim = require('easy-online-sim');
      const eOS = new EasyOnlineSim({token: "****" , requestInterval: 5000 });
      eOS.getNumInstagram()
           .then(tzIdObj => {
               // операции с номером tzIdOb.number
               return eOS.getState(tzIdObj.id);
           })
           .then(tzIdObjData => {
               console.log(tzIdObjData.msg) // <-- код здесь
               return eOS.done(tzIdObjData.id);
           })
           .then(responseDone => console.log(responseDone)
           .catch(console.error);
```
<a name="getNum"></a>

## getNum(service) ⇒ <code>Promise.&lt;0bject&gt;</code>
Метод запроса номера для определенного сервиса у онлайнсим, для получения номера телефона необходимо вызвать метод getState(tzId, false) false передает ответ о номере , но не повторяет запросы для прочтения пришедшего на него смс

**Kind**: global function  
**Returns**: <code>Promise.&lt;0bject&gt;</code> - объект с tzid, для получения по tzid  
**Link**: http://onlinesim.ru/docs/api/ru#getnum8  

| Param | Type | Description |
| --- | --- | --- |
| service | <code>string</code> | название сервиса |

<a name="getNumInstagram"></a>

## getNumInstagram() ⇒ <code>Promise.&lt;object.&lt;string, number, string&gt;&gt;</code>
Запрос номера в onlinesim

**Kind**: global function  
**Returns**: <code>Promise.&lt;object.&lt;string, number, string&gt;&gt;</code> - { type: 'number', id: '3686165', number: '+79681956382' }  
<a name="getState"></a>

## getState(id, repeat) ⇒ <code>Promise.&lt;TResult&gt;</code>
Метод, позволяющий получить состояние по запршенной операции, повторяется циклично пока не будет прочитано смс, таймут берется из конструктора класса

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| id | <code>number</code> | tzid выданный на сервис+номер для работы с сервисом |
| repeat | <code>bool</code> | флаг необходимости повтора запросов |

<a name="done"></a>

## done(id) ⇒ <code>Promise.&lt;TResult&gt;</code>
Метод выполняет закрытие операции с положительным результатом, под капотом setOperationOk

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| id | <code>number</code> | tzid выданный на сервис+номер для работы с сервисом |

<a name="abort"></a>

## abort(id) ⇒ <code>Promise.&lt;TResult&gt;</code>
Метод выполняет закрытие операции с тикетом о неверных данных, под капотом setOperationRevise

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| id | <code>number</code> | tzid выданный на сервис+номер для работы с сервисом |

<a name="balance"></a>

## balance() ⇒ <code>Promise.&lt;Object&gt;</code>
Метод запрашивает баланс по кабинету, при успешном выполнении запроса response = 1

**Kind**: global function  
**Returns**: <code>Promise.&lt;Object&gt;</code> - объект с данными по балансу {response:RESPONSE, "balance":"BALANCE", "zbalance":"ZBALANCE"}  
